﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using System.Web;
using System.Text;
using Newtonsoft.Json;

namespace JsonToManifestXml
{
    class SerIt
    {
        private string filename;
        private string dataPath;
        private string manifestver;

        private List<LevelHolder> LevelHoldList;

        public SerIt(string filename, string dataPath,string manifestver)
        {
            this.filename = filename;
            this.dataPath = dataPath;
            this.manifestver = manifestver;
        }

        private string[] getAllFiles(string path)
        {
            return Directory.GetFiles(path, "*.json");
        }

        private void ProcFiles(string path)
        {
            string[] gaga = getAllFiles(path);
            if (getAllFiles(path) != null)
            {
                foreach(string fileName in gaga)
                {

                    string fe = File.ReadAllText(fileName);
                    SerializableExtras extra = SerializableExtras.CreateFromJSON(fe);


                    LevelHolder l1 = new LevelHolder();
                    l1.id = extra.LevelNumber;
                    l1.version = extra.LevelVersion;
                    l1.Description = extra.LevelName;

                    LevelHoldList.Add(l1);
                }
            }
        }

        public void ExecSer()
        {
            LevelHoldList = new List<LevelHolder>();

            ProcFiles(dataPath);

            string ff = filename;
            // Create an instance of the XmlSerializer class;
            // specify the type of object to serialize.
            XmlSerializer serializer =
            new XmlSerializer(typeof(UpdateManifest));
            TextWriter writer = new StreamWriter(ff);
            UpdateManifest up = new UpdateManifest();
            
            up.Levels = LevelHoldList.ToArray();
            up.ManifestVersion = Int32.Parse(manifestver);

            serializer.Serialize(writer, up);
            writer.Close();
        }
    }

    [XmlRoot("UpdateManifest", IsNullable = false)]
    public class UpdateManifest
    {
        [XmlArray("Levels")]
        public LevelHolder[] Levels { get; set; }
        
        public int ManifestVersion;
    }

    [XmlType(TypeName = "Level")]
    public class LevelHolder
    {
        [XmlAttribute]
        public int id;
        [XmlAttribute]
        public int version;
        [XmlElement]
        public string Description;
    }

[Serializable]
    public class SerializableExtras
    {

        //Level Eigentschaften und Daten
        public string LevelName;
        public string BackgroundColor;
        public string RectColor;
        public int LevelDauer; //Anzahl an Balken, die durchlaufen werden muss
        public float Speed;
        public int LevelNumber; //Diese darf nicht zweimal vorkommen
        public string Schwierigkeit_x; //Je nachdem wir die Werte sind spawnen. Das wird so ein range Bereich zwischen 0 und 100% bei Viewport Screen. Enthält eine Range als float array.
        public string Schwierigkeit_y; //Je nachdem wir die Werte sind spawnen. Das wird so ein range Bereich zwischen 0 und 100% bei Viewport Screen. Enthält eine Range als float array.
        public float point_multilier = 1; //Jetzt auch in den Extras definiert: Fixes #34

        public int LevelVersion = 0;
        //Es muss noch die Möglichkeit geschaffen werden exakte Level zu generieren. Zum Beispiel alle RECTS positionen usw. und auch die der Items zu definieren. Bis dahin aber erstsmal per procedurale generiert.
        //Hier immer neue Extras hinzufügen
        public ScoreObjectSeri[] ScoreObjectSeri = { };
        public SteuerungsumkehrSeri[] SteuerungsumkehrSeri = { };
        public StroboskopSeri[] StroboskopSeri = { };
        public JumpingBallSeri[] JumpingBallSeri = { };


        public static SerializableExtras CreateFromJSON(string jsonString)
        {
            return JsonConvert.DeserializeObject<SerializableExtras>(jsonString);
        }
    }

    /// <summary>
    /// Hier sind alle Klassen, welche die Extras Serializable machen.
    /// @spawnTime Diese Variable dient dazu anzugeben, wann ein Extra in der Levelzeit gespawnt wird
    /// @dauer Diese Variable dient der Extradauer im Level nachdem spawnen
    /// @moveSpeed Diese Variable gibt die Geschwindigkeit des Extras an
    /// </summary>

    [Serializable]
    public class ScoreObjectSeri
    {
        public float spawnTime;
        public int dauer;
        public float movSpeed;
        public int amount;
        public string add_or_multiply;
        public float multiplay_dauer; //Wird einfach auf null gesetzt bei addy
    }


    [Serializable]
    public class SteuerungsumkehrSeri
    {
        public float spawnTime;
        public int dauer;
        public float movSpeed;
        public int speed;
        public int richtung; //1 oder 0
        public int steuerDauer;
    }

    [Serializable]
    public class StroboskopSeri
    {
        public float spawnTime;
        public int dauer;
        public float movSpeed;
        public float changeSpeed;
        public int strobDauer;
    }

    [Serializable]
    public class JumpingBallSeri
    {
        public float spawnTime;
        public int dauer;
        public float movSpeed;
        public float bounciness;
        public int jumpDauer;
    }
}
